package com.example.couroutinepractice_alex_ato.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.couroutinepractice_alex_ato.model.MainRepo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

class MainViewModel : ViewModel() {
    //    Call Repo
    private val mainRepo = MainRepo

    //    Create Mutable VARIABLES
    private var _chosenColor = MutableLiveData<Int>()
    val chosenColor: LiveData<Int> get() = _chosenColor

    private var _color = MutableLiveData<Int>()
    val color: LiveData<Int> get() = _color

    private var _lifecycleNamesList = MutableLiveData<List<String>>()
    val lifecycleNamesList: LiveData<List<String>> get() = _lifecycleNamesList

    private var _lifecycleInfoList = MutableLiveData<List<String>>()
    val lifecycleInfoList: LiveData<List<String>> get() = _lifecycleInfoList
// Mutable VARIABLES END

    // ViewModel Functions accessing Repo
    fun getRandomColor() {
        viewModelScope.launch {
            _color.value = mainRepo.getRandomColor()
        }
    }

    fun setColor(color: Int) {
        mainRepo.setRandomColor(color)
        _chosenColor.value = mainRepo.repoColor
    }

    fun getLifecycleInfo() = viewModelScope.launch(Dispatchers.Main) {
        _lifecycleInfoList.value = mainRepo.getLifecycleInfo()
    }

    fun getLifecycleNames() = viewModelScope.launch(Dispatchers.Main) {
        _lifecycleNamesList.value = mainRepo.getLifecycleNames()
    }

}