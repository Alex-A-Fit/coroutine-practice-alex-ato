package com.example.couroutinepractice_alex_ato.fragments

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.navigation.navArgs
import com.example.couroutinepractice_alex_ato.R
import com.example.couroutinepractice_alex_ato.databinding.FragmentColorBinding
import com.example.couroutinepractice_alex_ato.databinding.FragmentLifecycleChosenBinding
import com.example.couroutinepractice_alex_ato.viewmodel.MainViewModel

class LifecycleChosenFragment : Fragment() {
    private var _binding: FragmentLifecycleChosenBinding? = null
    private val binding: FragmentLifecycleChosenBinding get() = _binding!!

    private val viewmodel by activityViewModels<MainViewModel> ()
    private val args by navArgs<LifecycleChosenFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentLifecycleChosenBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onInit()
        Handler(Looper.getMainLooper()).postDelayed(
            {
                onInitListener()
            },
            2500
        )
    }

    private fun onInit() = with(binding) {
            viewmodel.getLifecycleInfo()
            viewmodel.getLifecycleNames()

    }

    private fun onInitListener() = with(binding) {
        val info = viewmodel.lifecycleInfoList.value!!
        val names = viewmodel.lifecycleNamesList.value!!
        tvLifecycleTitle.text = names[args.infoIndex]
        tvLifecycleDescription.text = info[args.infoIndex]
        backBtn.setOnClickListener {
            val toLifecyclePage = LifecycleChosenFragmentDirections.actionLifecycleChosenFragmentToLifecycleFragment()
            findNavController().navigate(toLifecyclePage)
        }
    }
}