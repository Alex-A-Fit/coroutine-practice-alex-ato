package com.example.couroutinepractice_alex_ato.fragments

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.couroutinepractice_alex_ato.R
import com.example.couroutinepractice_alex_ato.databinding.FragmentColorBinding
import com.example.couroutinepractice_alex_ato.databinding.FragmentLifecycleBinding
import com.example.couroutinepractice_alex_ato.viewmodel.MainViewModel

class LifecycleFragment : Fragment() {
    private var _binding: FragmentLifecycleBinding? = null
    private val binding: FragmentLifecycleBinding get() = _binding!!

    val viewmodel by activityViewModels<MainViewModel> ()

    private lateinit var lifeCycleNamesList: List<String>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentLifecycleBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onInit()
        Handler(Looper.getMainLooper()).postDelayed(
            {
                onInitListener()
            },
            2500
        )
    }

    private fun onInit() {
        viewmodel.getLifecycleNames()
    }

    private fun onInitListener() = with(binding) {
        var index: Int
        val goToColorFrag = LifecycleFragmentDirections.actionLifecycleFragmentToColorFragment()
        lifeCycleNamesList = viewmodel.lifecycleNamesList.value!!
        backBtn.setOnClickListener {
            findNavController().navigate(goToColorFrag)
        }

        lifecyclesBtn.setOnClickListener {
            textView.text = lifeCycleNamesList[0]
            textView2.text = lifeCycleNamesList[1]
            textView3.text = lifeCycleNamesList[2]
            textView4.text = lifeCycleNamesList[3]
            textView5.text = lifeCycleNamesList[4]
            textView6.text = lifeCycleNamesList[5]
            textView7.text = lifeCycleNamesList[6]
            tvLifecycle1.text = lifeCycleNamesList[7]
            textView9.text= lifeCycleNamesList[8]
            textView10.text = lifeCycleNamesList[9]
            textView11.text = lifeCycleNamesList[10]
            textView12.text = lifeCycleNamesList[11]
            textView13.text = lifeCycleNamesList[12]
        }
        textView.setOnClickListener {
            index = 0
            val goToCycleChosen = LifecycleFragmentDirections.actionLifecycleFragmentToLifecycleChosenFragment(index)
            findNavController().navigate(goToCycleChosen)
        }
        textView2.setOnClickListener {
            index = 1
            val goToCycleChosen = LifecycleFragmentDirections.actionLifecycleFragmentToLifecycleChosenFragment(index)
            findNavController().navigate(goToCycleChosen)
        }
        textView3.setOnClickListener {
            index = 2
            val goToCycleChosen = LifecycleFragmentDirections.actionLifecycleFragmentToLifecycleChosenFragment(index)
            findNavController().navigate(goToCycleChosen)
        }
        textView4.setOnClickListener {
            index = 3
            val goToCycleChosen = LifecycleFragmentDirections.actionLifecycleFragmentToLifecycleChosenFragment(index)
            findNavController().navigate(goToCycleChosen)
        }
        textView5.setOnClickListener {
            index = 4
            val goToCycleChosen = LifecycleFragmentDirections.actionLifecycleFragmentToLifecycleChosenFragment(index)
            findNavController().navigate(goToCycleChosen)
        }
        textView6.setOnClickListener {
            index = 5
            val goToCycleChosen = LifecycleFragmentDirections.actionLifecycleFragmentToLifecycleChosenFragment(index)
            findNavController().navigate(goToCycleChosen)
        }
        textView7.setOnClickListener {
            index = 6
            val goToCycleChosen = LifecycleFragmentDirections.actionLifecycleFragmentToLifecycleChosenFragment(index)
            findNavController().navigate(goToCycleChosen)
        }
        tvLifecycle1.setOnClickListener {
            index = 7
            val goToCycleChosen = LifecycleFragmentDirections.actionLifecycleFragmentToLifecycleChosenFragment(index)
            findNavController().navigate(goToCycleChosen)
        }
        textView9.setOnClickListener {
            index = 8
            val goToCycleChosen = LifecycleFragmentDirections.actionLifecycleFragmentToLifecycleChosenFragment(index)
            findNavController().navigate(goToCycleChosen)
        }
        textView10.setOnClickListener {
            index = 9
            val goToCycleChosen = LifecycleFragmentDirections.actionLifecycleFragmentToLifecycleChosenFragment(index)
            findNavController().navigate(goToCycleChosen)
        }
        textView11.setOnClickListener {
            index = 10
            val goToCycleChosen = LifecycleFragmentDirections.actionLifecycleFragmentToLifecycleChosenFragment(index)
            findNavController().navigate(goToCycleChosen)
        }
        textView12.setOnClickListener {
            index = 11
            val goToCycleChosen = LifecycleFragmentDirections.actionLifecycleFragmentToLifecycleChosenFragment(index)
            findNavController().navigate(goToCycleChosen)
        }
        textView13.setOnClickListener {
            index = 12
            val goToCycleChosen = LifecycleFragmentDirections.actionLifecycleFragmentToLifecycleChosenFragment(index)
            findNavController().navigate(goToCycleChosen)
        }
    }

}