package com.example.couroutinepractice_alex_ato.fragments

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.couroutinepractice_alex_ato.databinding.FragmentColorBinding
import com.example.couroutinepractice_alex_ato.viewmodel.MainViewModel


class ColorFragment : Fragment() {
    private var _binding: FragmentColorBinding? = null
    private val binding: FragmentColorBinding get() = _binding!!

    val viewmodel by activityViewModels<MainViewModel> ()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentColorBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewmodel.chosenColor.observe(viewLifecycleOwner) {
            binding.cvRandomColor.setCardBackgroundColor(it)
        }
        onInitListener()

    }

    private fun onInitListener() = with(binding) {
        colorBtn.setOnClickListener {
            viewmodel.getRandomColor()
            Handler(Looper.getMainLooper()).postDelayed(
                {
                    cvRandomColor.setCardBackgroundColor(viewmodel.color.value!!)
                },
                2500
            )
        }

        cvRandomColor.setOnClickListener {
            viewmodel.setColor(binding.cvRandomColor.cardBackgroundColor.defaultColor)
            val action = ColorFragmentDirections.actionColorFragmentToColorChosenFragment()
            findNavController().navigate(action)
        }

        nextBtn.setOnClickListener {
            val action = ColorFragmentDirections.actionColorFragmentToLifecycleFragment()
            findNavController().navigate(action)
        }

    }
}