package com.example.couroutinepractice_alex_ato.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.couroutinepractice_alex_ato.R
import com.example.couroutinepractice_alex_ato.databinding.FragmentColorBinding
import com.example.couroutinepractice_alex_ato.databinding.FragmentColorChosenBinding
import com.example.couroutinepractice_alex_ato.viewmodel.MainViewModel

class ColorChosenFragment : Fragment() {
    private var _binding: FragmentColorChosenBinding? = null
    private val binding: FragmentColorChosenBinding get() = _binding!!

    val viewmodel by activityViewModels<MainViewModel> ()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentColorChosenBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onInitListener()

    }

    private fun onInitListener() = with(binding){
        viewmodel.chosenColor.observe(viewLifecycleOwner) {
            colorChosenLayout.setBackgroundColor(it)
        }

        backBtn.setOnClickListener {
            val action = ColorChosenFragmentDirections.actionColorChosenFragmentToColorFragment()
            findNavController().navigate(action)
        }
    }
}