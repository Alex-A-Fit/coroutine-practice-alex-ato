package com.example.couroutinepractice_alex_ato.model.`interface`

import java.util.*

interface MainInt {
    suspend fun getRandomColor(): Int
    fun setRandomColor(color: Int)
    suspend fun getLifecycleInfo(): List<String>
    suspend fun getLifecycleNames(): List<String>
}