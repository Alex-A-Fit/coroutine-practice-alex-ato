package com.example.couroutinepractice_alex_ato.model

import android.graphics.Color
import com.example.couroutinepractice_alex_ato.model.`interface`.MainInt
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext
import kotlin.random.Random

object MainRepo {
    var repoColor: Int = 0
        get() = field
        set(value) {
            field = value
        }

    private val apiRepo = object : MainInt {

        override suspend fun getRandomColor(): Int {
            return Color.rgb(Random.nextInt(255), Random.nextInt(255), Random.nextInt(255))
        }

        override fun setRandomColor(color: Int) {
            repoColor = color
        }

        override suspend fun getLifecycleInfo(): List<String> {
            return listOf(
                "Called when a Fragment is first attached to a host Activity . Use this method to check if the Activity has implemented the required listener callback for the Fragment (if a listener interface was defined in the Fragment ). After this method, onCreate() is called.",
                "It is called when the activity is first created. This is where all the static work is done like creating views, binding data to lists, etc. This method also provides a Bundle containing its previous frozen state, if there was one",
                "Called to have the fragment instantiate its user interface view. This is optional, and non-graphical fragments can return null. This will be called between onCreate and onViewCreated.",
                "Called immediately after onCreateView has returned, but before any saved state has been restored in to the view. This gives subclasses a chance to initialize themselves once they know their view hierarchy has been completely created. The fragment's view hierarchy is not however attached to its parent at this point.",
                "Called when all saved state has been restored into the view hierarchy of the fragment. This can be used to do initialization based on saved state that you are letting the view hierarchy track itself, such as whether check box widgets are currently checked. This is called after onViewCreated and before onStart.",
                "When the activity enters the Started state, the system invokes this callback. The onStart() call makes the activity visible to the user, as the app prepares for the activity to enter the foreground and become interactive. For example, this method is where the app initializes the code that maintains the UI",
                "When the activity enters the Resumed state, it comes to the foreground, and then the system invokes the onResume() callback. This is the state in which the app interacts with the user. The app stays in this state until something happens to take focus away from the app.",
                "The system calls this method as the first indication that the user is leaving your activity (though it does not always mean the activity is being destroyed); it indicates that the activity is no longer in the foreground (though it may still be visible if the user is in multi-window mode).",
                "When your activity is no longer visible to the user, it has entered the Stopped state, and the system invokes the onStop() callback. This may occur, for example, when a newly launched activity covers the entire screen. The system may also call onStop() when the activity has finished running, and is about to be terminated.",
                "Calls the start method, refer to onStart lifecycle",
                "Called when the view previously created by onCreateView has been detached from the fragment. The next time the fragment needs to be displayed, a new view will be created. This is called after onStop and before onDestroy. It is called regardless of whether onCreateView returned a non-null view. Internally it is called after the view's state has been saved but before it has been removed from its parent.",
                "When the activity moves to the destroyed state, any lifecycle-aware component tied to the activity's lifecycle will receive the ON_DESTROY event. This is where the lifecycle components can clean up anything it needs to before the Activity is destroyed.",
                "Called when the fragment is no longer attached to its activity. This is called after onDestroy."
            )
        }


        override suspend fun getLifecycleNames(): List<String> {
            return listOf(
                "onAttach",
                "onCreate",
                "onCreateView",
                "onViewCreated",
                "onViewStateRestored",
                "onStart",
                "onResume",
                "onPause",
                "onStop",
                "onRestart",
                "onViewDestroyed",
                "onDestroy",
                "onDetach",
            )
        }
    }
    suspend fun getRandomColor(): Int = withContext(Dispatchers.IO) {
        delay(1000)
        return@withContext apiRepo.getRandomColor()
    }
    suspend fun getLifecycleInfo(): List<String> = withContext(Dispatchers.IO) {
        delay(1000L)
        return@withContext apiRepo.getLifecycleInfo()
    }
    fun setRandomColor(color: Int) {
        apiRepo.setRandomColor(color)
    }
    suspend fun getLifecycleNames(): List<String> = withContext(Dispatchers.IO){
        delay(1000L)
        return@withContext apiRepo.getLifecycleNames()
    }

}